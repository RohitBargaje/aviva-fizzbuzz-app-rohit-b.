﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FizzBuzzEnitityFramework;
using FizzBuzzWebApi.Models;

namespace FizzBuzzWebApi.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values/5
        public bool Get(int number)
        {
            AddValues addValues = new AddValues();
            return addValues.StoreNumberToDB(number);
        }

        // POST api/values
        [HttpPost]
        public void Post(FizzBuzzModel model)
        {
            AddValues addValues = new AddValues();
            addValues.StoreNumberToDB(model.Number);
        }
    }    
}