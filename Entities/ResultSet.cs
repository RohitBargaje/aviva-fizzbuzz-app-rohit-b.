﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class ResultSet
    {
        public int Number { get; set; }
        public string FizzBizz { get; set; }
    }
}
