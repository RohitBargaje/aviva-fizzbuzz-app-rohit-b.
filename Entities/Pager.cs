﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class Pager
    {
        public int PageSize { get; set; }
        public int PageNo { get; set; }
        public int TotalPages { get; set; }
    }
}
