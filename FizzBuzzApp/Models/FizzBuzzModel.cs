﻿using System;
using System.Collections.Generic;
using Entities;

namespace FizzBuzzApp.Models
{
    public class FizzBuzzModel
    {
        public int Number { get; set; }
        public List<ResultSet> ResultList { get; set; }
        public Pager ObjPager { get; set; }
    }    
}