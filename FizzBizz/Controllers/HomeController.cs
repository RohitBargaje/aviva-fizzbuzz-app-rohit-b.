﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcApplication5.Models;
using System.Web.Helpers;
using Entities;
using UtilityLayer;
using System.Web.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;

namespace MvcApplication5.Controllers
{
    public class HomeController : Controller
    {
        #region Public Method
        
        /// <summary>
        /// Load serach panel.
        /// </summary>
        /// <returns></returns>
        public ActionResult Home()
        {
            FizzBuzzModel model = new FizzBuzzModel();
            model.Number = 1;
            return View(model);
        }

        /// <summary>
        /// Http get method for paging
        /// </summary>
        /// <param name="pageNo"></param>
        /// <param name="number"></param>
        /// <returns></returns>
        public ActionResult SearchResult(int pageNo, int number)
        {
            return PartialView(Constant.SearchResultView, GetFizzBuzzModel(number, pageNo));
        }

        /// <summary>
        /// Http post method for filling web grid.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SearchResult(FizzBuzzModel model)
        {
            return PartialView(Constant.SearchResultView, GetFizzBuzzModel(model.Number));
        }

        #endregion

        #region Private Method

        /// <summary>
        /// Fill FizzBizz model.
        /// </summary>
        /// <param name="number"></param>
        /// <param name="pageNo"></param>
        /// <returns></returns>
        private FizzBuzzModel GetFizzBuzzModel(int number, int pageNo = 1) 
        {
            FizzBuzzModel fizzBuzzModel = new FizzBuzzModel();
            fizzBuzzModel.Number = number;
            if(fizzBuzzModel.ResultList == null)
            {
                fizzBuzzModel.ResultList = new List<ResultSet>();
            }
            if (fizzBuzzModel.ObjPager == null)
            {
                fizzBuzzModel.ObjPager = new Pager();
                fizzBuzzModel.ObjPager.PageNo = pageNo;
                fizzBuzzModel.ObjPager.PageSize = Constant.PageSize;
                var totalPages = (int)Math.Ceiling((Convert.ToDouble(number) / Convert.ToDouble(Constant.PageSize)));
                fizzBuzzModel.ObjPager.TotalPages = totalPages == 0 ? 1 : totalPages;
            }
            try
            {
                int pageSize = Constant.PageSize;
                int startValue = (pageNo * pageSize) - (pageSize - 1);
                int endValue = number <= (pageNo * pageSize) ? number : (pageNo * pageSize);

                if (DateTime.Now.DayOfWeek == DayOfWeek.Wednesday)
                {
                }
                for (int i = startValue; i <= endValue; i++)
                {
                    string strFizzBuzz = string.Empty;

                    if (i % 3 == 0 && i % 5 == 0)
                    {
                        strFizzBuzz = DateTime.Now.DayOfWeek == DayOfWeek.Wednesday ? Constant.WizzWuzz : Constant.FizzBuzz;
                    }
                    else if (i % 3 == 0)
                    {
                        strFizzBuzz = DateTime.Now.DayOfWeek == DayOfWeek.Wednesday ? Constant.Wizz : Constant.Fizz;
                    }
                    else if (i % 5 == 0)
                    {
                        strFizzBuzz = DateTime.Now.DayOfWeek == DayOfWeek.Wednesday ? Constant.Wuzz : Constant.Buzz;
                    }
                    else
                    {
                        strFizzBuzz = i.ToString();
                    }
                    fizzBuzzModel.ResultList.Add(new ResultSet() { Number = i, FizzBizz = strFizzBuzz });
                }
            }
            catch(Exception ex) 
            {
                throw new Exception("FizzBuzz calculation error - {0}", ex.InnerException);
            }

            return fizzBuzzModel;
        }

        #endregion
    }
}
