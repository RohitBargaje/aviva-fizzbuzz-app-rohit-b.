﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Data.SqlClient;

namespace FizzBuzzEnitityFramework
{
    public class AddValues
    {
        public bool StoreNumberToDB(int number)
        {
            try
            {
                using (var context = new masterEntities())
                {
                    StoreValue values = context.StoreValues.OrderByDescending(c => c.SequenceNumber).FirstOrDefault();
                    int newId = (null == values ? 0 : values.SequenceNumber) + 1;
                    StoreValue storeValues = new StoreValue();
                    storeValues.SequenceNumber = newId;
                    storeValues.SearchNumber = number;

                    context.StoreValues.Add(storeValues);
                    var result = context.SaveChanges() > 0;
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }
        }
    }
}
