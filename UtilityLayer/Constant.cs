﻿using System;

namespace UtilityLayer
{
    public static class Constant
    {
        public const string Fizz = "Fizz";
        public const string Buzz = "Buzz";
        public const string FizzBuzz = "FizzBuzz";
        public const string Wizz = "Wizz";
        public const string Wuzz = "Wuzz";
        public const string WizzWuzz = "WizzWuzz";
        public const string SearchResultView = "SearchResult";
        public const int PageSize = 20;
    }
}
