﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using MvcApplication5.Controllers;
using MvcApplication5.Models;
using NUnit.Framework;

namespace FizzBuzzTest.Tests
{
    [TestFixture]
    public class HomeControllerTest
    {
        [Test]
        public void TestSearchResultViewName() 
        {
            HomeController controller = new HomeController();
            FizzBuzzModel model = new FizzBuzzModel() { Number = 27 };
            var result = controller.SearchResult(model) as PartialViewResult;

            Assert.AreEqual("SearchResult", result.ViewName);
        }

        [Test]
        public void TestSearchResultCount()
        {
            HomeController controller = new HomeController();
            FizzBuzzModel model = new FizzBuzzModel() { Number = 27 };
            var result = controller.SearchResult(model) as PartialViewResult;
            var resultModel = (FizzBuzzModel)result.Model;
            var count = resultModel.ResultList.Count.ToString();
            Assert.AreEqual("20", count);
        }
    }
}
